package com.lasich.dao;

import com.lasich.model.RoutineData;

import java.util.Calendar;
import java.util.List;

public interface RoutineDao {

    void deleteRoutine(int routineId);

    void save(RoutineData routineData);
/*
    List<RoutineData> getRoutinesFromDate(int userId, Calendar startDate);

    List<RoutineData> getRoutinesOnDate(int userId, java.util.Date date);
*/
    List<RoutineData> getAllRoutines(int userId);

}
