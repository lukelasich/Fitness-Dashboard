package com.lasich.dao;

import com.lasich.model.RoutineData;
import com.lasich.model.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.*;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.Query;
import org.hibernate.Query.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


@Repository("routineDao")
public class RoutineDaoImpl extends AbstractDao<Integer, RoutineData> implements RoutineDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void deleteRoutine(int routineId)
    {
        String sql = "delete from routines where ROUTINE_ID=?";
        jdbcTemplate.update(sql, new Object[]
                { routineId });
    }

    public void save(RoutineData routineData) {
        persist(routineData);
    }

    /*
    public List<RoutineData> getRoutinesFromDate(int userId, Calendar startDate)
    {
        String sql = "select * from routines WHERE USER_ID=? AND DATE>=?";

        List<RoutineData> routineList = jdbcTemplate.query(sql, new Object[] {userId, startDate}, new ResultSetExtractor<List<RoutineData>>()
        {
            @Override
            public List<RoutineData> extractData(ResultSet rs) throws SQLException, DataAccessException
            {
                List<RoutineData> list = new ArrayList<RoutineData>();
                while (rs.next())
                {
                    RoutineData routineData = new RoutineData();
                    routineData.setRoutine_id(rs.getInt("ROUTINE_ID"));
                    routineData.setRoutineDate(rs.getDate("DATE"));
                    routineData.setUnitData(rs.getInt("UNITS"));
                    routineData.setTimePeriod(rs.getTime("TIME_PERIOD"));
                    routineData.setDescription(RoutineData.Description.valueOf(rs.getString("DESCRIPTION").toUpperCase()));
                    list.add(routineData);
                }
                return list;
            }

        });
        return routineList;
    }
    */
/*
    public List<RoutineData> getRoutinesOnDate(int userId, java.util.Date date)
    {
        String sql = "select * from routines WHERE USER_ID=? AND DATE=?";

        List<RoutineData> routineList = jdbcTemplate.query(sql, new Object[] {userId, date}, new ResultSetExtractor<List<RoutineData>>()
        {
            @Override
            public List<RoutineData> extractData(ResultSet rs) throws SQLException, DataAccessException
            {
                List<RoutineData> list = new ArrayList<RoutineData>();
                while (rs.next())
                {
                    RoutineData routineData = new RoutineData();
                    routineData.setRoutine_id(rs.getInt("ROUTINE_ID"));
                    routineData.setRoutineDate(rs.getDate("DATE"));
                    routineData.setUnitData(rs.getInt("UNITS"));
                    routineData.setTimePeriod(rs.getTime("TIME_PERIOD"));
                    routineData.setDescription(RoutineData.Description.valueOf(rs.getString("DESCRIPTION").toUpperCase()));
                    list.add(routineData);
                }
                return list;
            }

        });
        return routineList;
    }
*/



    @SuppressWarnings("unchecked")
    public List<RoutineData> getAllRoutines(int userId) {

        Criteria criteria = createEntityCriteria().addOrder(Order.asc("routineDate"));
        //Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("user_id", userId));
        //criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);//To avoid duplicates.
        System.out.println(" >>> int userId): " + userId);
        List<RoutineData> routineDataList = (List<RoutineData>) criteria.list();




        System.out.println(" >>> getAllRoutines(int userId): " + routineDataList.get(0).getUnitData());



        return routineDataList;
    }

}
