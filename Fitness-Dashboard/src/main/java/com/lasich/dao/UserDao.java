package com.lasich.dao;

import java.util.List;

import com.lasich.model.User;


public interface UserDao {

	User findById(int id);
	
	User findByAccountName(String accountName);
	
	void save(User user);
	
	void deleteByAccountName(String accountName);
	
	List<User> findAllUsers();

}

