package com.lasich.model;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="ROUTINES")
public class RoutineData implements Serializable
{

    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer routine_id;

    @NotNull
    @Column(name="USER_ID", nullable=false)
    private Integer user_id;

    @Column(name="DEVICE_ID")
    private Integer device_id;

    @NotNull
    @Column(name="DATE", nullable=false)
    private java.sql.Date routineDate;

    @NotNull
    @Column(name="UNITS", nullable=false)
    private int unitData;

    @Column(name="TIME_PERIOD")
    private Time timePeriod;

    @NotEmpty
    @Column(name="DESCRIPTION", nullable=false)
    private String routineDescription;

    /*
    @Enumerated(EnumType.STRING)
    private Gender gender;

    public enum Description {
        RUNNING("Running"),
        STEPS("Steps"),
        HEARTRATE("HeartRate");

        private final String value;
        Description(final String value){
            this.value = value;
        }
        public String getValue(){ return value;}
    }
*/

    public Integer getRoutine_id() { return routine_id; }

    public void setRoutine_id(Integer routine_id) { this.routine_id = routine_id; }


    public String getDescription() {return routineDescription;}

    public void setDescription(String routineDescription) {
        this.routineDescription = routineDescription;
    }

    public Integer getUser_id() {return user_id;}

    public void setUser_id(Integer user_id) {this.user_id = user_id;}

    public int getUnitData() {
        return unitData;
    }

    public void setUnitData(int unitData) {
        this.unitData = unitData;
    }

    public Time getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(Time timePeriod) {
        this.timePeriod = timePeriod;
    }

    public java.sql.Date getRoutineDate() {
        return routineDate;
    }

    public void setRoutineDate(java.sql.Date routineDate) {
        this.routineDate = routineDate;
    }

    public Integer getDevice_id() {return device_id;}

    public void setDevice_id(Integer device_id) {this.device_id = device_id;}

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof RoutineData))
            return false;
        RoutineData other = (RoutineData) obj;
        if (user_id == null) {
            if (other.routine_id != null)
                return false;
        } else if (!routine_id.equals(other.routine_id))
            return false;
        return true;
    }

}

