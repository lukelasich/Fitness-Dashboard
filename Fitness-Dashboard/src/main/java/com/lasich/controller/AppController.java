package com.lasich.controller;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.lasich.model.RoutineData;
import com.lasich.model.UserProfile;
import com.lasich.service.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import com.lasich.model.User;
import com.lasich.service.UserService;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class AppController {

	@Autowired
	UserService userService;
	
	@Autowired
	UserProfileService userProfileService;
	
	@Autowired
	MessageSource messageSource;

	@Autowired
	PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;
	
	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;
	
	
	/**
	 * This method will list all existing users.
	 */
	/*
	@RequestMapping(value = { "/", "/list" }, method = RequestMethod.GET)
	public String listUsers(ModelMap model) {

		System.out.println("Got here. App controller listUsers");
		List<User> users = userService.findAllUsers();
		model.addAttribute("users", users);
		model.addAttribute("loggedinuser", getPrincipal());
		return "userslist";
	}
	*/

	@RequestMapping(value = { "/", "/home", "/user" }, method = RequestMethod.GET)
	public String homePage(ModelMap model) {

		System.out.println("Got here. App controller homePage");
		//List<User> users = userService.findAllUsers();
		RoutineData routineData = new RoutineData();
		model.addAttribute("loggedinuser", getPrincipalAccountName());
		int loggedInUserId = userService.findByAccountName(getPrincipalAccountName()).getUser_id();
		System.out.println("loggedInUserId: " + loggedInUserId);
		User user = userService.findById(loggedInUserId);
		List<RoutineData> routineList = userService.getAllRoutines(loggedInUserId);
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
		routineData.setRoutineDate(date);
		model.addAttribute("user", user);
		model.addAttribute("routineList", routineList);
		model.addAttribute("routineData", routineData);

		return "user";
	}


	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public String saveRoutine(ModelMap model, @RequestParam("userId") int userId, @ModelAttribute("saveRoutine")
			RoutineData routineData, @RequestParam("button") String buttonVal) {

		if ("Submit Distance".equals(buttonVal)) {
			routineData.setDescription("Running");
		} else if ("Submit Steps Taken".equals(buttonVal)) {
			routineData.setDescription("Steps");
		} else if ("Submit Heart Rate".equals(buttonVal)) {
			routineData.setDescription("Heart Rate");
		}

		routineData.setUser_id(userId);
		routineData.setDevice_id(99);

		System.out.println("Got this far.... <<<<");
		userService.saveRoutine(routineData);

		User user = userService.findById(userId);
		List<RoutineData> routineList = userService.getAllRoutines(user.getUser_id());
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
		routineData.setRoutineDate(date);
		routineData.setTimePeriod(null);
		//double calories = fitnessService.calcLastRoutineCalories(user);
		//model = new ModelAndView("user");
		model.addAttribute("user", user);
		model.addAttribute("routineData", routineData);
		model.addAttribute("routineList", routineList);
		//mav.addObject("lastRoutineCalories", calories);

		return "user";
	} /* public ModelAndView saveRoutine(@RequestParam("userId") int userId, @ModelAttribute("saveRoutine")
            RoutineData routineData, @RequestParam("button") String buttonVal) */
	/**
	 * This method will provide the medium to add a new user.
	 */
	/*
	@RequestMapping(value = { "/newuser" }, method = RequestMethod.GET)
	public String newUser(ModelMap model) {
		User user = new User();
		model.addAttribute("user", user);
		model.addAttribute("edit", false);
		model.addAttribute("loggedinuser", getPrincipalAccountName());
		return "registration";
	}*/

	@RequestMapping(value = "/newuser", method = RequestMethod.GET)
	public String newUser(ModelMap model) {
		System.out.println("inside new user! ???????????");
		User user = new User();
		model.addAttribute("user", user);
		return "registration";
	}

	/**
	 * This method will be called on form submission, handling POST request for
	 * saving user in database. It also validates the user input
	 */

	@RequestMapping(value = { "/register" }, method = RequestMethod.POST)
	public String saveUser(@Valid User user, BindingResult result,
			ModelMap model) {

		UserProfile userProfile = new UserProfile();
		userProfile.setType("USER");
		userProfile.setUser_id(1);
		Set<UserProfile> userProfiles = new HashSet<>(2);
		userProfiles.add(userProfile);
		user.setUserProfiles(userProfiles);


		if (result.getErrorCount() > 1) {
			return "registration";
		}

		System.out.println(user.getUserProfiles());
		/*
		 * Preferred way to achieve uniqueness of field [sso] should be implementing custom @Unique annotation 
		 * and applying it on field [sso] of Model class [User].
		 * 
		 * Below mentioned peace of code [if block] is to demonstrate that you can fill custom errors outside the validation
		 * framework as well while still using internationalized messages.
		 * 
		 */

		if(!userService.isUserAccountNameUnique(user.getUser_id(), user.getAccountName())){
			FieldError accountNameError =new FieldError("user","accountName",messageSource.getMessage("non.unique.accountName", new String[]{user.getAccountName()}, Locale.getDefault()));
		    result.addError(accountNameError);
			return "registration";
		}
		
		userService.saveUser(user);

		model.addAttribute("message", "User " + user.getFirstName() + " "+ user.getLastName() + " registered successfully");

		//return "success";
		return "login";
	}


	/**
	 * This method will provide the medium to update an existing user.
	 */
	@RequestMapping(value = { "/edit-user-{accountName}" }, method = RequestMethod.GET)
	public String editUser(@PathVariable String accountName, ModelMap model) {
		User user = userService.findByAccountName(accountName);
		model.addAttribute("user", user);
		model.addAttribute("edit", true);
		model.addAttribute("loggedinuser", getPrincipalAccountName());
		return "registration";
	}
	
	/**
	 * This method will be called on form submission, handling POST request for
	 * updating user in database. It also validates the user input
	 */
	@RequestMapping(value = { "/edit-user-{accountName}" }, method = RequestMethod.POST)
	public String updateUser(@Valid User user, BindingResult result,
			ModelMap model, @PathVariable String accountName) {

		if (result.hasErrors()) {
			return "registration";
		}

		/*//Uncomment below 'if block' if you WANT TO ALLOW UPDATING SSO_ID in UI which is a unique key to a User.
		if(!userService.isUserAccountNameUnique(user.getUser_id(), user.getAccountName())){
			FieldError ssoError =new FieldError("user","ssoId",messageSource.getMessage("non.unique.ssoId", new String[]{user.getAccountName()}, Locale.getDefault()));
		    result.addError(ssoError);
			return "registration";
		}*/


		userService.updateUser(user);

		model.addAttribute("success", "User " + user.getFirstName() + " "+ user.getLastName() + " updated successfully");
		model.addAttribute("loggedinuser", getPrincipalAccountName());
		return "registrationsuccess";
	}

	
	/**
	 * This method will delete an user by it's SSOID value.
	 */
	@RequestMapping(value = { "/delete-user-{accountName}" }, method = RequestMethod.GET)
	public String deleteUser(@PathVariable String accountName) {
		userService.deleteUserByAccountName(accountName);
		return "redirect:/list";
	}
	

	/**
	 * This method will provide UserProfile list to views
	 */
	@ModelAttribute("roles")
	public List<UserProfile> initializeProfiles() {
		return userProfileService.findAll();
	}
	
	/**
	 * This method handles Access-Denied redirect.
	 */
	@RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
	public String accessDeniedPage(ModelMap model) {
		model.addAttribute("loggedinuser", getPrincipalAccountName());
		return "accessDenied";
	}

	/**
	 * This method handles login GET requests.
	 * If users is already logged-in and tries to goto login page again, will be redirected to list page.
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage() {
		System.out.println("Got here. Request /login, loginPage.");
		if (isCurrentAuthenticationAnonymous()) {
			return "login";
	    } else {
	    	return "redirect:/home";
	    }
	}

	/**
	 * This method handles logout requests.
	 * Toggle the handlers if you are RememberMe functionality is useless in your app.
	 */
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logoutPage (HttpServletRequest request, HttpServletResponse response){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null){    
			//new SecurityContextLogoutHandler().logout(request, response, auth);
			persistentTokenBasedRememberMeServices.logout(request, response, auth);
			SecurityContextHolder.getContext().setAuthentication(null);
		}
		return "redirect:/login?logout";
	}

	/**
	 * This method returns the principal[user-name] of logged-in user.
	 */
	private String getPrincipalAccountName(){
		String accountName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			accountName = ((UserDetails)principal).getUsername();
		} else {
			accountName = principal.toString();
		}
		return accountName;
	}

	
	/**
	 * This method returns true if users is already authenticated [logged-in], else false.
	 */
	private boolean isCurrentAuthenticationAnonymous() {
		System.out.println(" calling isCurrentAuthenticationAnonymous()");
	    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    return authenticationTrustResolver.isAnonymous(authentication);
	}


}