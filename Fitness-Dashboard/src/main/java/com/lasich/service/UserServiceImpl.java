package com.lasich.service;

import java.util.List;

import com.lasich.dao.RoutineDao;
import com.lasich.dao.UserDao;
import com.lasich.model.RoutineData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lasich.model.User;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao userdao;

	@Autowired
	private RoutineDao routineDao;

	@Autowired
    private PasswordEncoder passwordEncoder;
	
	public User findById(int id) {
		return userdao.findById(id);
	}

	public User findByAccountName(String accountName) {
		User user = userdao.findByAccountName(accountName);
		return user;
	}

	public void saveUser(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		userdao.save(user);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate update explicitly.
	 * Just fetch the entity from db and update it with proper values within transaction.
	 * It will be updated in db once transaction ends. 
	 */
	public void updateUser(User user) {
		User entity = userdao.findById(user.getUser_id());
		if(entity!=null){
			entity.setAccountName(user.getAccountName());
			if(!user.getPassword().equals(entity.getPassword())){
				entity.setPassword(passwordEncoder.encode(user.getPassword()));
			}
			entity.setFirstName(user.getFirstName());
			entity.setLastName(user.getLastName());
			entity.setGender(user.getGender());
			entity.setUserProfiles(user.getUserProfiles());
		}
	}

	public void saveRoutine(RoutineData routineData) {routineDao.save(routineData);}
	
	public void deleteUserByAccountName(String accountName) {
		userdao.deleteByAccountName(accountName);
	}

	public List<User> findAllUsers() {
		return userdao.findAllUsers();
	}

	public List<RoutineData> getAllRoutines(int userId) { return routineDao.getAllRoutines(userId); }

	public boolean isUserAccountNameUnique(Integer id, String accountName) {
		User user = findByAccountName(accountName);
		return ( user == null || ((id != null) && (user.getUser_id() == id)));
	}
	
}
