package com.lasich.service;

import java.util.List;

import com.lasich.model.RoutineData;
import com.lasich.model.User;


public interface UserService {
	
	User findById(int id);
	
	User findByAccountName(String accountName);
	
	void saveUser(User user);

	void saveRoutine(RoutineData routineData);
	
	void updateUser(User user);
	
	void deleteUserByAccountName(String accountName);

	List<User> findAllUsers();

	List<RoutineData> getAllRoutines(int userId);
	
	boolean isUserAccountNameUnique(Integer id, String accountName);

}