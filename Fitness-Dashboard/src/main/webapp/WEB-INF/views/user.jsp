<!DOCTYPE html SYSTEM "http://www.thymeleaf.org/dtd/xhtml1-strict-thymeleaf-spring4-4.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>


<head>

    <title>Loso - Simple, Clean And Responsive Website!!! </title>

    <meta charset="utf-8">

    <meta name="description"
          content="Simple, clean,123123123 responsive website built with html5, CSS3, Js, jQuery and Bootstrap">

    <meta name="keywords" content="web, design, html, css, html5, css3, javascript, jquery, bootstrap, development">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:500italic,600italic,600,700,700italic,300italic,300,400,400italic,800,900'
          rel='stylesheet' type='text/css'>

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,300italic,400italic,600italic,700,900'
          rel='stylesheet' type='text/css'>

    <!-- favicon -->
    <link rel="icon" type="image/png" href="static/images/favicon.png">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="static/css/bootstrap.min.css">

    <!-- Fontawesome  -->
    <link rel="stylesheet" href="static/css/font-awesome.min.css">

    <!-- Animate CSS -->
    <link rel="stylesheet" type="text/css" href="static/css/animate.css">

    <link rel="stylesheet" type="text/css" href="static/css/style.css">

    <!-- BX slider CSS -->
    <link rel="stylesheet" type="text/css" href="static/css/jquery.bxslider.css">

    <!-- responsive css -->
    <link rel="stylesheet" type="text/css" href="static/css/responsive.css">


</head>

<body>

<!-- Header -->
<header class="header" id="HOME">


    <!-- Navigation -->

    <nav class="navbar navbar-default navbar-fixed-top">


        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#loso-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="loso-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#HOME" class="nav-item">HOME</a></li>
                    <li><a href="#GOALS" class="nav-item">GOALS</a></li>
                </ul>


            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>


    <div class="header-overlay">

        <div class="container header-container">

            <div class="row">
                <div class="col-md-6">
                    <div class="header-text">
                        <!-- Title & Description -->
                        <h1> Fitness Dashboard</h1>
                        <h3 style="color: white"> Welcome back ${user.firstName} </h3>
                        <h4> <span class="floatRight"><a href="<c:url value="/logout" />">Logout</a></span> </h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"
                         style="width: 375px">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                       aria-expanded="true" aria-controls="collapseOne">
                                        Enter A Running Routine
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <form:form id="usersForm" modelAttribute="routineData" action="user"
                                               method="post">
                                        <input type="hidden" name="userId" value="${user.user_id}">
                                        <table class="table table-condensed" style="width:300px">
                                            <tr>
                                                <td><form:label path="routineDate">Routine Date:</form:label></td>
                                                <td><form:input path="routineDate" name="routineDate" id="routineDate"
                                                                placeholder="CCYY-MM-DD"/></td>
                                            </tr>
                                            <tr>
                                                <td><form:label path="unitData">Distance Ran:</form:label></td>
                                                <td><form:input path="unitData" name="unitData" id="unitData"
                                                                placeholder="metres"/></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <form:label path="timePeriod">Time Period:</form:label>
                                                </td>
                                                <td>
                                                    <form:input path="timePeriod" name="timePeriod" id="timePeriod"
                                                                placeholder="hh:mm:ss"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="submit" name="button" class="btn btn-primary btn-block" value="Submit Distance"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </form:form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Enter A Steps Routine
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <form:form id="usersForm" modelAttribute="routineData" action="user"
                                               method="post">
                                        <input type="hidden" name="userId" value="${user.user_id}">
                                        <table class="table table-condensed" style="width:300px">
                                            <tr>
                                                <td><form:label path="routineDate">Routine Date:</form:label></td>
                                                <td><form:input path="routineDate" name="routineDate" id="routineDate"
                                                                placeholder="CCYY-MM-DD"/></td>
                                            </tr>
                                            <tr>
                                                <td><form:label path="unitData">No. of Steps:</form:label></td>
                                                <td><form:input path="unitData" name="unitData" id="unitData"
                                                                placeholder="steps"/></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <form:label path="timePeriod">Time Period:</form:label>
                                                </td>
                                                <td>
                                                    <form:input path="timePeriod" name="timePeriod" id="timePeriod"
                                                                placeholder="hh:mm:ss"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="submit" name="button" class="btn btn-primary btn-block" value="Submit Steps Taken"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </form:form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Enter A Heart Rate Routine
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <form:form id="usersForm" modelAttribute="routineData" action="user"
                                               method="post">
                                        <input type="hidden" name="userId" value="${user.user_id}">
                                        <table class="table table-condensed" style="width:300px">
                                            <tr>
                                                <td><form:label path="routineDate">Routine Date:</form:label></td>
                                                <td><form:input path="routineDate" name="routineDate" id="routineDate"
                                                                placeholder="CCYY-MM-DD"/></td>
                                            </tr>
                                            <tr>
                                                <td><form:label path="unitData">Heart Rate:</form:label></td>
                                                <td><form:input path="unitData" name="unitData" id="unitData"
                                                                placeholder="bpm"/></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <form:label path="timePeriod">Time Period:</form:label>
                                                </td>
                                                <td>
                                                    <form:input path="timePeriod" name="timePeriod" id="timePeriod"
                                                                placeholder="hh:mm:ss"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="submit" name="button" class="btn btn-primary btn-block" value="Submit Heart Rate"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </form:form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="col-md-6 ">
                    <div class="panel panel-routines" style="height: 450px; overflow: auto">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-7">
                                    <h3>Your routines</h3>
                                    <table class="table table-condensed" style="width:520px">
                                        <tr>
                                            <th align="left">Routine ID</th>
                                            <th align="left">Routine Date</th>
                                            <th align="left">Unit Data</th>
                                            <th align="left">Time Period</th>
                                            <th align="left">Description</th>
                                        </tr>
                                        <c:forEach items="${routineList}" var="routineDataList">
                                            <tr>
                                                <td align="left">${routineDataList.routine_id}</td>
                                                <td align="left">${routineDataList.routineDate}</td>
                                                <td align="left">${routineDataList.unitData}</td>
                                                <td align="left">${routineDataList.timePeriod}</td>
                                                <td align="left">${routineDataList.description}</td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-3">
                        <form id="analytics-form" action="/analytics" method="post" role="form"
                              style="display: block;">
                            <div class="form-group">
                                <input type="hidden" name="userId" value="${user.user_id}">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <input type="submit" name="analytics-submit" id="analytics-submit" tabindex="4"
                                           class="btn btn-primary btn-block" value="Analytics">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <i class="fa fa-angle-down"></i>
            </a>

        </div>
    </div>
    </div>


</header>

<section class="goals">

    <div class="goals-overlay">

        <div class="container">

            <div class="row">

                <div class="col-md-12 wow bounceInDown">


                    <div id="carousel-goals" class="carousel slide" data-ride="carousel">


                        <!-- indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-analytics" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-analytics" data-slide-to="1"></li>
                            <li data-target="#carousel-analytics" data-slide-to="2"></li>
                        </ol>


                        <!-- wrapper for slides -->
                        <div class="carousel-inner">


                            <!-- item 01 -->
                            <div class="item active text-center">


                                <h2> some kind of analytics goes here</h2>

                            </div>

                            <!-- item 02 -->
                            <div class="item text-center">


                                <h2> another kind of analytics goes here</h2>


                            </div>


                            <!-- item 03 -->
                            <div class="item text-center">


                            </div>




                        </div>


                    </div>



                </div>


            </div>


        </div>


    </div>

</section>
<!-- Footer -->
<footer class="footer">

    <div class="container">

        <div class="row">

            <div class="col-md-6">

                <div id="copyright">
                    <p>Copyright &copy; 2016 <a href="#"> - LoSo</a></p>
                </div>
            </div>
            <div class="col-md-6">

                <div class="scroll-top">
                    <a href="#HOME" id="scroll-to-top"><i class="fa fa-angle-up"></i></a>
                </div>
            </div>


        </div>

    </div>


</footer>

<!-- Bootstrap JavaScript -->
<script src="static/js/jquery.js"></script>
<script src="static/js/bootstrap.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js"></script>

<!-- Bx Slider JS -->
<script src="static/js/jquery.bxslider.min.js"></script>

<!-- Add JS cstaticounter lib -->
<script src="static/js/jquery.waypoints.min.js"></script>
<script src="static/js/jquery.counterup.min.js"></script>

<!-- Add wow js lib -->
<script src="static/js/wow.min.js"></script>

<!-- Custom Js -->
<script src="static/js/custom.js"></script>


</body>


</html>