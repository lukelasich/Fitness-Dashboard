<!DOCTYPE html SYSTEM "http://www.thymeleaf.org/dtd/xhtml1-strict-thymeleaf-spring4-4.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>


<head>

    <title>Loso - Simple, Clean And Responsive Website!!! </title>

    <meta charset="utf-8">

    <meta name="description"
          content="Simple, clean,123123123 responsive website built with html5, CSS3, Js, jQuery and Bootstrap">

    <meta name="keywords" content="web, design, html, css, html5, css3, javascript, jquery, bootstrap, development">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:500italic,600italic,600,700,700italic,300italic,300,400,400italic,800,900'
          rel='stylesheet' type='text/css'>

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,300italic,400italic,600italic,700,900'
          rel='stylesheet' type='text/css'>

    <!-- favicon -->
    <link rel="icon" type="image/png" href="static/images/favicon.png">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="static/css/bootstrap.min.css">

    <!-- Fontawesome  -->
    <link rel="stylesheet" href="static/css/font-awesome.min.css">

    <!-- Animate CSS -->
    <link rel="stylesheet" type="text/css" href="static/css/animate.css">

    <link rel="stylesheet" type="text/css" href="static/css/style.css">

    <!-- BX slider CSS -->
    <link rel="stylesheet" type="text/css" href="static/css/jquery.bxslider.css">

    <!-- responsive css -->
    <link rel="stylesheet" type="text/css" href="static/css/responsive.css">


</head>


<body>

<!-- Header -->
<header class="header" id="HOME">

    <!-- Navigation -->

    <nav class="navbar navbar-default navbar-fixed-top">


        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#loso-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="loso-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#HOME" class="nav-item">HOME</a></li>
                </ul>


            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>


    <div class="header-overlay">

        <div class="container header-container">

            <div class="row">

                <div class="col-md-8 col-md-offset-4">

                    <div class="header-text">

                        <!-- Title & Description -->
                        <h1> Fitness Dashboard</h1>

                        <div class="col-md-offset-1">
                            <p> Motivation through analytics </p>
                        </div>

                    </div>

                    <div class="container" style="opacity: 0.88">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="panel panel-login">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <a href="/login" class="active" id="login-form-link">Login</a>
                                            </div>
                                            <div class="col-xs-6">
                                                <a href="#" id="register-form-link">Register</a>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <form:form method="POST" modelAttribute="user" class="form-horizontal" action="register">
                                                    <form:input type="hidden" path="user_id" id="user_id"/>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label class="col-md-6 control-label" for="firstName">Account Name</label>
                                                            <div class="col-md-6">
                                                                <form:input type="text" path="accountName" user_id="accountName" class="form-control input-sm"/>
                                                                <div class="has-error">
                                                                    <form:errors path="accountName" class="help-inline"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label class="col-md-6 control-label" for="password">Password</label>
                                                            <div class="col-md-6">
                                                                <form:input type="password" path="password" user_id="password" class="form-control input-sm" />
                                                                <div class="has-error">
                                                                    <form:errors path="password" class="help-inline"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label class="col-md-6 control-label" for="firstName">First Name</label>
                                                            <div class="col-md-6">
                                                                <form:input type="text" path="firstName" user_id="firstName" class="form-control input-sm"/>
                                                                <div class="has-error">
                                                                    <form:errors path="firstName" class="help-inline"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label class="col-md-6 control-label" for="lastName">Last Name</label>
                                                            <div class="col-md-6">
                                                                <form:input type="text" path="lastName" user_id="lastName" class="form-control input-sm" />
                                                                <div class="has-error">
                                                                    <form:errors path="lastName" class="help-inline"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label class="col-md-6 control-label" for="gender">Gender</label>
                                                            <div class="col-md-6">
                                                                <form:input type="text" path="gender" user_id="gender" class="form-control input-sm" />
                                                                <div class="has-error">
                                                                    <form:errors path="gender" class="help-inline"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label class="col-md-6 control-label" for="age">Age</label>
                                                            <div class="col-md-6">
                                                                <form:input type="number" path="age" user_id="age" class="form-control input-sm" />
                                                                <div class="has-error">
                                                                    <form:errors path="age" class="help-inline"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label class="col-md-6 control-label" for="height">Height</label>
                                                            <div class="col-md-6">
                                                                <form:input type="number" path="height" user_id="height" class="form-control input-sm" />
                                                                <div class="has-error">
                                                                    <form:errors path="height" class="help-inline"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label class="col-md-6 control-label" for="weight">Weight</label>
                                                            <div class="col-md-6">
                                                                <form:input type="number" path="weight" user_id="weight" class="form-control input-sm" />
                                                                <div class="has-error">
                                                                    <form:errors path="weight" class="help-inline"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-actions col-md-12">
                                                            <input type="submit" value="Register" class="btn btn-primary btn-sm"/>
                                                        </div>
                                                    </div>
                                                </form:form>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <i class="fa fa-angle-down"></i>
                    </a>

                </div>

            </div>

        </div>

    </div>


</header>


</section>
<!-- Footer -->
<footer class="footer">

    <div class="container">

        <div class="row">

            <div class="col-md-6">

                <div id="copyright">
                    <p>Copyright &copy; 2016 <a href="#"> - LoSo</a></p>
                </div>
            </div>
            <div class="col-md-6">

                <div class="scroll-top">
                    <a href="#HOME" id="scroll-to-top"><i class="fa fa-angle-up"></i></a>
                </div>
            </div>


        </div>

    </div>


</footer>

<!-- Bootstrap JavaScript -->
<script src="static/js/jquery.js"></script>
<script src="static/js/bootstrap.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- Bx Slider JS -->
<script src="static/js/jquery.bxslider.min.js"></script>

<!-- Add JS counter lib -->
<script src="static/js/jquery.waypoints.min.js"></script>
<script src="static/js/jquery.counterup.min.js"></script>

<!-- Add wow js lib -->
<script src="static/js/wow.min.js"></script>

<!-- Custom Js -->
<script src="static/js/custom.js"></script>


</body>


</html>