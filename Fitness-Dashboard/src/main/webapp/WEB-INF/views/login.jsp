<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html>
<head>

    <title>Loso - Simple, Clean And Responsive Website!!! </title>

    <meta charset="utf-8">

    <meta name="description"
          content="Simple, clean,123123123 responsive website built with html5, CSS3, Js, jQuery and Bootstrap">

    <meta name="keywords" content="web, design, html, css, html5, css3, javascript, jquery, bootstrap, development">

    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:500italic,600italic,600,700,700italic,300italic,300,400,400italic,800,900'
          rel='stylesheet' type='text/css'>

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,300italic,400italic,600italic,700,900'
          rel='stylesheet' type='text/css'>

    <!-- favicon -->
    <link rel="icon" type="image/png" href="static/images/favicon.png">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="static/css/bootstrap.min.css">

    <!-- Fontawesome  -->
    <link rel="stylesheet" href="static/css/font-awesome.min.css">

    <!-- Animate CSS -->
    <link rel="stylesheet" type="text/css" href="static/css/animate.css">

    <link rel="stylesheet" type="text/css" href="static/css/style.css">

    <!-- BX slider CSS -->
    <link rel="stylesheet" type="text/css" href="static/css/jquery.bxslider.css">

    <!-- responsive css -->
    <link rel="stylesheet" type="text/css" href="static/css/responsive.css">


</head>


<body>

<!-- Header -->
<header class="header" id="HOME">

    <!-- Navigation -->

    <nav class="navbar navbar-default navbar-fixed-top">


        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#loso-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="loso-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#HOME" class="nav-item">HOME</a></li>
                    <li><a href="#TESTIMONIALS" class="nav-item">TESTIMONIALS</a></li>
                </ul>


            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>


    <div class="header-overlay">

        <div class="container header-container">

            <div class="row">

                <div class="col-md-8 col-md-offset-4">

                    <div class="header-text">

                        <!-- Title & Description -->
                        <h1> Fitness Dashboard</h1>

                        <div class="col-md-offset-1">
                            <p> Motivation through analytics </p>
                        </div>

                    </div>

                    <div class="container" style="opacity: 0.88">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="panel panel-login">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <a href="#" class="active" id="login-form-link">Login</a>
                                            </div>
                                            <div class="col-xs-6">
                                                <a href="/newuser" id="register-form-link">Register</a>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div user_id="mainWrapper">
                                                <div class="login-container">
                                                    <div class="login-card">
                                                        <div class="login-form">
                                                            <c:url var="loginUrl" value="/login"/>
                                                            <form action="${loginUrl}" method="post"
                                                                  class="form-horizontal">
                                                                <c:if test="${param.error != null}">
                                                                    <div class="alert alert-danger">
                                                                        <p>Invalid username and password.</p>
                                                                    </div>
                                                                </c:if>
                                                                <c:if test="${param.logout != null}">
                                                                    <div class="alert alert-success">
                                                                        <p>You have been logged out successfully.</p>
                                                                    </div>
                                                                </c:if>
                                                                <div class="input-group input-sm">
                                                                    <label class="input-group-addon"
                                                                           for="accountName"><i class="fa fa-user"></i></label>
                                                                    <input type="text" class="form-control"
                                                                           id="accountName" name="accountName"
                                                                           placeholder="Enter Username" required>
                                                                </div>
                                                                <div class="input-group input-sm">
                                                                    <label class="input-group-addon" for="password"><i
                                                                            class="fa fa-lock"></i></label>
                                                                    <input type="password" class="form-control"
                                                                           id="password" name="password"
                                                                           placeholder="Enter Password" required>
                                                                </div>
                                                                <div class="input-group input-sm">
                                                                    <div class="checkbox">
                                                                        <label><input type="checkbox" id="rememberme"
                                                                                      name="remember-me"> Remember
                                                                            Me</label>
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" name="${_csrf.parameterName}"
                                                                       value="${_csrf.token}"/>

                                                                <div class="form-actions">
                                                                    <input type="submit"
                                                                           class="btn btn-block btn-primary btn-default"
                                                                           value="Log in">
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>

        </div>

    </div>

</header>


<!-- Testimonials -->
<section class="testimonial">

    <div class="testimonial-overlay">

        <div class="container">

            <div class="row">

                <div class="col-md-12 wow bounceInDown">


                    <div id="carousel-testimonial" class="carousel slide" data-ride="carousel">


                        <!-- indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-testimonial" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-testimonial" data-slide-to="1"></li>
                            <li data-target="#carousel-testimonial" data-slide-to="2"></li>
                        </ol>


                        <!-- wrapper for slides -->
                        <div class="carousel-inner">


                            <!-- item 01 -->
                            <div class="item active text-center">


                                <img src="images/client-01.jpg" alt="testimonial" class="center-block">

                                <div class="testimonial-caption">

                                    <h2>Daniel</h2>
                                    <h4><span> Sr. Software Engineer, </span>Blue Gyms</h4>
                                    <p>“Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip.”</p>

                                </div>


                            </div>

                            <!-- item 02 -->
                            <div class="item text-center">


                                <img src="images/client-02.jpg" alt="testimonial" class="center-block">

                                <div class="testimonial-caption">

                                    <h2>Rauls Pole</h2>
                                    <h4><span> Marketting Manager, </span>Risko</h4>
                                    <p>“Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip.”</p>

                                </div>


                            </div>


                            <!-- item 03 -->
                            <div class="item text-center">


                                <img src="images/client-03.jpg" alt="testimonial" class="center-block">

                                <div class="testimonial-caption">

                                    <h2>Jim So</h2>
                                    <h4><span> Assistan Manager, </span>Gidfo</h4>
                                    <p>“Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip.”</p>

                                </div>


                            </div>


                        </div>


                    </div>


                </div>


            </div>


        </div>


    </div>

</section>

<!-- Footer -->
<footer class="footer">

    <div class="container">

        <div class="row">

            <div class="col-md-6">

                <div id="copyright">
                    <p>Copyright &copy; 2016 <a href="#"> - LoSo</a></p>
                </div>
            </div>
            <div class="col-md-6">

                <div class="scroll-top">
                    <a href="#HOME" id="scroll-to-top"><i class="fa fa-angle-up"></i></a>
                </div>
            </div>


        </div>

    </div>


</footer>

<!-- Bootstrap JavaScript -->
<script src="static/js/jquery.js"></script>
<script src="static/js/bootstrap.min.js"></script>

<!-- Bx Slider JS -->
<script src="static/js/jquery.bxslider.min.js"></script>

<!-- Add JS counter lib -->
<script src="static/js/jquery.waypoints.min.js"></script>
<script src="static/js/jquery.counterup.min.js"></script>

<!-- Add wow js lib -->
<script src="static/js/wow.min.js"></script>

<!-- Custom Js -->
<script src="static/js/custom.js"></script>

</body>


</html>