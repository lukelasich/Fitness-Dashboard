<!DOCTYPE html SYSTEM "http://www.thymeleaf.org/dtd/xhtml1-strict-thymeleaf-spring4-4.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>


<head>

    <title>Loso - Simple, Clean And Responsive Website!!! </title>

    <meta charset="utf-8">

    <meta name="description"
          content="Simple, clean,123123123 responsive website built with html5, CSS3, Js, jQuery and Bootstrap">

    <meta name="keywords" content="web, design, html, css, html5, css3, javascript, jquery, bootstrap, development">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:500italic,600italic,600,700,700italic,300italic,300,400,400italic,800,900'
          rel='stylesheet' type='text/css'>

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,300italic,400italic,600italic,700,900'
          rel='stylesheet' type='text/css'>

    <!-- favicon -->
    <link rel="icon" type="image/png" href="images/favicon.png">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

    <!-- Fontawesome  -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- Animate CSS -->
    <link rel="stylesheet" type="text/css" href="css/animate.css">

    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- BX slider CSS -->
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">

    <!-- responsive css -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">


</head>


<body>

<!-- Header -->
<header class="header" id="HOME">

    <!-- Navigation -->

    <nav class="navbar navbar-default navbar-fixed-top">


        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#loso-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="loso-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#HOME" class="nav-item">HOME</a></li>
                </ul>


            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>


    <div class="header-overlay">

        <div class="container header-container">

            <div class="row">
                <div class="col-md-6">
                    <div class="header-text">
                        <!-- Title & Description -->
                        <h1> Fitness Dashboard</h1>
                        <h3 style="color: white"> Your Weekly Calorie Burn </h3>
                    </div>
                </div>
            </div>

            <canvas id="myChart" width="200" height="200"></canvas>

            <i class="fa fa-angle-down"></i>
            </a>

        </div>
    </div>
    </div>


</header>


<!-- Footer -->
<footer class="footer">

    <div class="container">

        <div class="row">

            <div class="col-md-6">

                <div id="copyright">
                    <p>Copyright &copy; 2016 <a href="#"> - LoSo</a></p>
                </div>
            </div>
            <div class="col-md-6">

                <div class="scroll-top">
                    <a href="#HOME" id="scroll-to-top"><i class="fa fa-angle-up"></i></a>
                </div>
            </div>


        </div>

    </div>


</footer>

<!-- Bootstrap JavaScript -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js"></script>

<!-- Bx Slider JS -->
<script src="js/jquery.bxslider.min.js"></script>

<!-- Add JS counter lib -->
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>

<!-- Add wow js lib -->
<script src="js/wow.min.js"></script>

<!-- Custom Js -->
<script src="js/custom.js"></script>

<script>

    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["6 days ago","5 days ago","4 days ago","3 days ago","2 days ago","Yesterday", "Today"],
            datasets: [{
                label: 'Calories Burned',
                data: [${lastMonthsCalories[24]},${lastMonthsCalories[25]},${lastMonthsCalories[26]},${lastMonthsCalories[27]},${lastMonthsCalories[28]},${lastMonthsCalories[29]},${lastMonthsCalories[30]}],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
</script>

</body>


</html>
